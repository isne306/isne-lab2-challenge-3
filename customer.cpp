﻿#include <iostream>
#include "customer.h"
using namespace std;

int customer::get_ArrivalTime()
{
	return arrivaltime;
}

int customer::get_Servicetime()
{
	return servicetime;
}

int customer::get_WaitingTime()
{
	return waitingtime;
}

void customer::set_ArrivalTime(int t)
{
	arrivaltime = t;
}

void customer::set_ServiceTime(int t)
{
	servicetime = t;
}

void customer::set_WaitingTime(int t)
{
	waitingtime = t;
}

customer::customer(int arr)
{
	arrivaltime = arr;
	waitingtime = 0;
}

bool operator < (customer a, customer b)
{
	if (a.get_ArrivalTime() < b.get_ArrivalTime())
	{
		return true;
	}
	else
	{
		return false;
	}
}