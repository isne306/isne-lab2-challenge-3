﻿#ifndef CUSTOMER_H
#define CUSTOMER_H

class customer {

private:
	int waitingtime;
	int arrivaltime;
	int servicetime;

public:
	int get_WaitingTime();
	int get_ArrivalTime();
	int get_Servicetime();
	void set_WaitingTime(int t);
	void set_ArrivalTime(int t);
	void set_ServiceTime(int t);
	customer(int arr);
	friend bool operator <(customer a, customer b);
};

#endif