﻿#include<iostream>
#include<iomanip>
#include<ctime>
#include<queue>
#include<list>
#include "customer.h"

using namespace std;

int main()
{
	int	MaxCustomer, MinCustomer, MaxServiceTime, MinServiceTime, NumCashiers, NumCustomer=0,ServiceTime;
	float avg=0;
	list<customer> lst;
	queue<customer> result;
	list<int> finishedTime;

	srand(time(0));

		cout << "\n------------------------------------------"
			 << "\n- Welcome to the Bank Simulation Program -"
			 << "\n------------------------------------------";
	cout << endl;
	cout << endl;
	//Menu information
	cout << "Put number of Max Customer:" << endl;
	cin >> MaxCustomer;
	cout << "Put number of Min Customer:" << endl;
	cin >> MinCustomer;
	cout << "Put the Max Service time:" << endl;
	cin >> MaxServiceTime;
	cout << "Put the Min Service time:" << endl;
	cin >> MinServiceTime;
	cout << "Put number of Cashiers:" << endl;
	cin >> NumCashiers;


	NumCustomer = MinCustomer + rand() % (MaxCustomer - MinCustomer + 1);

	for (int i = 0; i < NumCustomer; i++)
	{
		customer a(rand() % 241);
		lst.push_back(a);
	}

	lst.sort();

	for (int i = 0; i < NumCashiers; i++){
		finishedTime.push_back(0);
	}

	for (int k = 0; k < NumCustomer; k++)
	{
		finishedTime.sort();
		int finish = finishedTime.front();

		int waitingtime=0;

		ServiceTime = MinServiceTime + rand() % (MaxServiceTime - MinServiceTime + 1);

		customer tmp = lst.front();
		if (finish < tmp.get_ArrivalTime() )
		{
			tmp.set_ServiceTime(ServiceTime);
			finishedTime.pop_front();
			finishedTime.push_front(tmp.get_Servicetime() + finish + tmp.get_ArrivalTime());
			result.push(tmp);
		}

		else
		{
			tmp.set_ServiceTime(ServiceTime);
			waitingtime=finish-tmp.get_ArrivalTime();
			avg = avg + waitingtime;
			tmp.set_WaitingTime(waitingtime);
			finishedTime.pop_front();
			finishedTime.push_front(tmp.get_Servicetime() + finish);
			result.push(tmp);
		}

		lst.pop_front();
	}

	cout << endl;
	cout << endl;

			//Output user input data
	cout << "\n---------------"
		 << "\n- Data Output -"
		 << "\n---------------\n";
	for (int i = 0; i < NumCustomer; i++){
		customer tmp = result.front();
		result.pop();
		cout << "Customer" << i + 1 << " - arrived " << tmp.get_ArrivalTime() 
		<< " - wait time " << tmp.get_WaitingTime() 
		<< " - service time " << tmp.get_Servicetime() 
		<< " - left " << (tmp.get_ArrivalTime() + tmp.get_Servicetime() + tmp.get_WaitingTime() )<< endl;
	}
cout << "\nAverage wait time - " << avg / NumCustomer << " minutes.";

	int n;
	cin >> n;
}